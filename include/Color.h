#ifndef COLOUR_H_
#define COLOUT_H_
#pragma once

#include <SDL2/SDL.h>

namespace frame2d
{
	struct Color : public SDL_Color {
	public:
		Color(SDL_Color& c);
	};
}

#endif
