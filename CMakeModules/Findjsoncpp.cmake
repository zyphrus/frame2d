# - Try to find JsonCPP

find_package(PkgConfig)
pkg_check_modules(JSONCPP QUIET jsoncpp)
set(JSONCPP_DEFINITIONS ${JSONCPP_CFLAGS_OTHER})

find_path(JSONCPP_INCLUDE_DIR jsoncpp/json/
          HINTS ${JSONCPP_INCLUDEDIR} ${JSONCPP_INCLUDE_DIRS}
          PATH_SUFFIXES jsoncpp/json )

find_library(JSONCPP_LIBRARY NAMES jsoncpp
             HINTS ${JSONCPP_LIBDIR} ${JSONCPP_LIBRARY_DIRS} )

set(JSONCPP_LIBRARIES ${JSONCPP_LIBRARY} )
set(JSONCPP_INCLUDE_DIRS ${JSONCPP_INCLUDE_DIR} )

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(jsoncpp  DEFAULT_MSG
                                  JSONCPP_LIBRARY JSONCPP_INCLUDE_DIR)

mark_as_advanced(JSONCPP_INCLUDE_DIR JSONCPP_LIBRARY )