project(Frame2D)
cmake_minimum_required(VERSION 2.8)

# Point to our own cmake modules
list(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/CMakeModules)

# Match build folder structure to src
set(EXECUTABLE_OUTPUT_PATH ${CMAKE_CURRENT_BINARY_DIR})
set(LIBRARY_OUTPUT_PATH ${CMAKE_CURRENT_BINARY_DIR})

# Find Libraries
find_package(SDL2 REQUIRED)
find_package(jsoncpp REQUIRED)

# Add global definitions
add_definitions("-std=c++11")

include_directories(${SDL2_INCLUDE_DIR})
include_directories(${JSONCPP_INCLUDE_DIR})
include_directories(${PROJECT_SOURCE_DIR}/include)

add_library(frame2d SHARED
	src/Timer.cpp
	src/Game.cpp
	src/Vector.cpp
	src/Point.cpp
	src/Rectangle.cpp
	src/Window.cpp
	src/Renderer.cpp
	src/Color.cpp
	src/Texture.cpp
)

target_link_libraries(frame2d ${SDL2_LIBRARIES})
target_link_libraries(frame2d ${JSONCPP_LIBRARIES})

set(FRAME2D_LIBRARY ${LIBRARY_OUTPUT_PATH}/libframe2d.so) #FIXME: so not only for Unix based

# Test Suite
find_package(UnitTest++ QUIET)
if (UNITTEST++_FOUND AND BUILD_TESTING)
	enable_testing()

	add_executable(frame2dTest
		tests/VectorTest.cpp
		tests/GameTest.cpp
		tests/SanityTest.cpp
		tests/runner.cpp
		tests/RectangleTest.cpp
		tests/RendererTest.cpp
		tests/WindowTest.cpp
		tests/ColorTest.cpp
		tests/PointTest.cpp
	)
	add_dependencies(frame2dTest frame2d)

	include_directories(${UNITTEST++_INCLUDE_DIR})

	target_link_libraries(frame2dTest ${FRAME2D_LIBRARY})
	target_link_libraries(frame2dTest ${SDL2_LIBRARIES})
	target_link_libraries(frame2dTest ${JSONCPP_LIBRARIES})
	target_link_libraries(frame2dTest ${UNITTEST++_LIBRARY})

	add_test(Frame2DTest frame2dTest)
endif()
